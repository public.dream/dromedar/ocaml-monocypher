# ocaml-monocypher

ocaml-monocypher provides OCaml bindings to the [Monocypher](https://monocypher.org/) cryptographic library.

Bindings are created using the [Ctypes](https://github.com/ocamllabs/ocaml-ctypes) library. Using dune to build a Ctypes project requires a bit of ceremony (well actually quite a lot - see [this issue](https://github.com/ocaml/dune/issues/135)). The boilerplate to make Ctypes and dune play nice has been taken from [ocaml-yaml](https://github.com/avsm/ocaml-yaml).

Currently only a small sub-set of Monocypher functions are available in the OCaml library (Blake2b, IETF ChaCha20 and Ed25519). Contributions to add bindings to the other Monocypher functions are very welcome!

# Why ocaml-monocypher?

Monocypher provides "Boring crypto that simply works". In particular it provides primitives required for [DROMEDAR](https://gitlab.com/public.dream/dromedar/dromedar):

- Blake2b cryptographic hash
- ChaCha20 stream cypher
- Ed25519 for public key signatures

Monocypher is portable, easy to build and include in projects (see also [Why Monocypher?](https://monocypher.org/why)).

## Why not [hacl-star](https://github.com/project-everest/hacl-star)?

HACL* is a formally verified cryptographic library that provides automatically generated OCaml bindings. All necessary primitives (Blake2b, ChaCha20, Ed25519) are provided by HACL*.

However, it does not seem trivial to build HACL* directly from source (F*). One needs to rely on pre-generated C and OCaml artifacts.

Furthermore, HACL* is considerably larger than Monocypher.

## Why not [mirage-crypto](https://github.com/mirage/mirage-crypto)?

mirage-crypto is a mostly OCaml implementation (with some C) of cryptographic primitives. It is maintained by the MirageOS community.

Unfortunately, mirage-crypto does not provide all necessary primitives (Blake2b and Ed25519 are missing).

If in the future the necessary crypto primitive are added to mirage-crypto it would make sense to deprecate ocaml-monocypher.

## Why not [vbmithr/ocaml-monocypher](https://github.com/vbmithr/ocaml-monocypher/tree/master/src)?

vbmithr/ocaml-monocypher does not use the Ctypes library for creating the binding. This requires some C hacking.

As the OCaml C stubs are directly in the `monocypher.c` file it is hard to update to a newer version of Monocypher.

# License

[./LICENSES/CC0-1.0.txt](CC0-1.0) for Monocypher.

[./LICENSES/AGPL-3.0-or-later.txt](AGPL-3.0-or-later) for OCaml bindings.
