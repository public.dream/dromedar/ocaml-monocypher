(use-modules
 (guix packages)
 (guix download)
 (guix git-download)
 (guix build-system dune)
 ((guix licenses) #:prefix license:)
 (gnu packages ocaml))

(define-public ocaml-monocypher
  (package
   (name "ocaml-monocypher")
   (version "0.0.0")
   (source #f)
   (build-system dune-build-system)
   (arguments '())
   (propagated-inputs
    `(("ocaml-integers" ,ocaml-integers)
      ("ocaml-ctypes" ,ocaml-ctypes)))
   (native-inputs
    `(("alcotest" ,ocaml-alcotest)))
   (home-page "https://gitlab.com/public.dream/DROMEDAR/ocaml-monocypher")
   (synopsis "OCaml bindings to the Monocypher cryptographic library")
   (description "Monocypher is a cryptographic library. It provides functions
for authenticated encryption, hashing, password hashing and key derivation, key
exchange, and public key signatures.  This library provides OCaml bindings to
Monocypher using Ctypes.")
   (license license:agpl3+)))

ocaml-monocypher
