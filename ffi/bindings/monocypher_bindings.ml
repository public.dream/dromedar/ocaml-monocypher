module M(F: Ctypes.FOREIGN) = struct
  let foreign = F.foreign

  module C = struct
    include Ctypes
    let (@->)         = F.(@->)
    let returning     = F.returning
  end

  let crypto_blake2b_general =
    foreign "crypto_blake2b_general"
      C.(ptr char @-> size_t @-> string_opt @-> size_t @-> string @-> size_t @-> returning void)

  let crypto_ietf_chacha20_ctr =
    foreign "crypto_ietf_chacha20_ctr"
      C.(ptr char @-> string @-> size_t @-> string @-> string @-> uint32_t @-> returning uint32_t)

  let crypto_ed25519_public_key =
    foreign "crypto_ed25519_public_key"
      C.(ptr char @-> string @-> returning void)

  let crypto_ed25519_sign =
    foreign "crypto_ed25519_sign"
      C.(ptr char @-> string @-> string @-> string @-> size_t @-> returning void)

  let crypto_ed25519_check =
    foreign "crypto_ed25519_check"
      C.(string @-> string @-> string @-> size_t @-> returning int)
end
