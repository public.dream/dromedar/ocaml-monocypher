(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)


open Ctypes

module M = Monocypher_ffi.M

module Hashing = struct
  module Blake2b = struct

    let digest ?key ?(size=64) msg =
      let hash_ptr = allocate_n char ~count:size in
      let key_size = Option.(key
                             |> map String.length
                             |> value ~default:0) in

      let () = M.crypto_blake2b_general
          hash_ptr (Unsigned.Size_t.of_int size)
          key (Unsigned.Size_t.of_int key_size)
          msg (Unsigned.Size_t.of_int (String.length msg))
      in

      string_from_ptr hash_ptr ~length:size
  end
end

module Advanced = struct
  module IETF_ChaCha20 = struct
    let crypt ~key ~nonce ?(ctr=0) msg =
      let cipher_ptr = allocate_n char ~count:(String.length msg) in
      let msg_size = String.length msg in
      let _next_ctr = M.crypto_ietf_chacha20_ctr
          cipher_ptr
          msg (Unsigned.Size_t.of_int msg_size)
          key
          nonce
          (Unsigned.UInt32.of_int ctr)
      in
      string_from_ptr cipher_ptr ~length:msg_size
  end
end

module Optional = struct
  module Ed25519 = struct

    let public_key ~secret_key =
      let public_key_ptr = allocate_n char ~count:32 in
      assert (String.length secret_key = 32);
      M.crypto_ed25519_public_key
        public_key_ptr
        secret_key;
      string_from_ptr public_key_ptr ~length:32

    let sign ~secret_key ?(public_key=public_key ~secret_key) message =
      let signature_ptr = allocate_n char ~count:64 in
      assert (String.length secret_key = 32);
      assert (String.length public_key = 32);
      M.crypto_ed25519_sign
        signature_ptr
        secret_key
        public_key
        message
        (Unsigned.Size_t.of_int @@ String.length message);
      string_from_ptr signature_ptr ~length:64

    let check ~signature ~public_key message =
      assert (String.length signature = 64);
      assert (String.length public_key = 32);
      let result = M.crypto_ed25519_check
          signature
          public_key
          message
          (Unsigned.Size_t.of_int @@ String.length message) in
      if result = 0 then
        true
      else
        false

  end
end
